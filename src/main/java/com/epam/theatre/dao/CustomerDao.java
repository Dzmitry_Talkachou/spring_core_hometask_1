package com.epam.theatre.dao;

import com.epam.theatre.domain.Customer;

public interface CustomerDao extends AbstractDomainObjectDao<Customer> {

	Customer getUserByEmail(String email);

	Customer takeByEmailPassword(String email, String password);

	void updateById(Long customerId, Customer customer);

}
