package com.epam.theatre.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.epam.theatre.dao.SqlFields;
import com.epam.theatre.dao.CustomerDao;
import com.epam.theatre.dao.impl.rowmapper.CustomerRowMapper;
import com.epam.theatre.domain.Customer;

@Repository
public class CustomerDaoImpl implements CustomerDao {

	private enum CoustomerSqlQuery {

		SQL_TAKE_ALL("select CUSTOMER_ID, FIRST_NAME, LAST_NAME, BIRTHDAY, EMAIL, PASSWORD from CUSTOMER"), //

		SQL_SAVE(
				"insert into CUSTOMER (CUSTOMER_ID, FIRST_NAME, LAST_NAME, BIRTHDAY, EMAIL, PASSWORD) values (CUSTOMER_SEQ.NEXTVAL,?,?,?,?,?)"), //

		SQL_REMOVE("delete from CUSTOMER where CUSTOMER_ID = ?"), //

		SQL_TAKE_BY_ID(
				"select CUSTOMER_ID, FIRST_NAME, LAST_NAME, EMAIL, BIRTHDAY, PASSWORD from CUSTOMER where CUSTOMER_ID = ?"), //

		SQL_TAKE_BY_EMAIL(
				"select CUSTOMER_ID, FIRST_NAME, LAST_NAME, EMAIL, BIRTHDAY, PASSWORD from CUSTOMER where EMAIL = ?"), //
		SQL_TAKE_BY_EMAIL_AND_PASSWORD(
				"select CUSTOMER_ID, FIRST_NAME, LAST_NAME, EMAIL, BIRTHDAY, PASSWORD from CUSTOMER where EMAIL = ? and PASSWORD = ?"), //
		SQL_UPDATE_BY_ID(
				"update CUSTOMER set FIRST_NAME = ?, LAST_NAME = ?, PASSWORD = ?, BIRTHDAY = ? WHERE CUSTOMER_ID = ?");

		final String query;

		CoustomerSqlQuery(String query) {
			this.query = query;
		}

	}

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Long save(Customer cutomer) {

		KeyHolder holder = new GeneratedKeyHolder();

		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

				PreparedStatement ps = connection.prepareStatement(CoustomerSqlQuery.SQL_SAVE.query,
						new String[] { SqlFields.CUSTOMER_ID.name() });
				ps.setString(1, cutomer.getFirstName());
				ps.setString(2, cutomer.getLastName());
				ps.setTimestamp(3, Timestamp.valueOf(cutomer.getBirthDay().atStartOfDay()));
				ps.setString(4, cutomer.getEmail());
				ps.setString(5, cutomer.getPassword());
				return ps;
			}
		}, holder);

		return holder.getKey().longValue();

	}

	@Override
	public void remove(Long cutomerId) {

		jdbcTemplate.update(CoustomerSqlQuery.SQL_REMOVE.query, cutomerId);
	}

	@Override
	public Customer getById(Long cutomerId) {

		try {
			return jdbcTemplate.queryForObject(CoustomerSqlQuery.SQL_TAKE_BY_ID.query, new Object[] { cutomerId },
					new CustomerRowMapper());

		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public List<Customer> getAll() {
		return jdbcTemplate.query(CoustomerSqlQuery.SQL_TAKE_ALL.query, new CustomerRowMapper());
	}

	@Override
	public Customer getUserByEmail(String cutomerEmail) {

		try {
			return jdbcTemplate.queryForObject(CoustomerSqlQuery.SQL_TAKE_BY_EMAIL.query, new Object[] { cutomerEmail },
					new CustomerRowMapper());
		} catch (EmptyResultDataAccessException e) {
			return null;
		}

	}

	@Override
	public Customer takeByEmailPassword(String email, String password) {

		Customer customer;
		try {
			customer = jdbcTemplate.queryForObject(CoustomerSqlQuery.SQL_TAKE_BY_EMAIL_AND_PASSWORD.query,
					new Object[] { email, password }, new CustomerRowMapper());
		} catch (EmptyResultDataAccessException e) {
			customer = null;
		}

		return customer;

	}

	@Override
	public void updateById(Long customerId, Customer customer) {

		jdbcTemplate.update(CoustomerSqlQuery.SQL_UPDATE_BY_ID.query, new Object[] { customer.getFirstName(),
				customer.getLastName(), customer.getPassword(), customer.getBirthDay(),

		});

	}

}
