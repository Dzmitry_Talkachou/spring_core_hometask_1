package com.epam.theatre.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.epam.theatre.dao.EventDao;
import com.epam.theatre.dao.SqlFields;
import com.epam.theatre.dao.impl.rowmapper.EventRowMapper;
import com.epam.theatre.domain.Event;

@Repository
public class EventDaoImpl implements EventDao {

	private enum EventDaoSqlQuery {

		SQL_TAKE_ALL("select EVENT_ID,BASE_PRICE,EVENT_NAME,EVENT_RATING from EVENT"), //

		SQL_SAVE("insert into EVENT (EVENT_ID, BASE_PRICE,EVENT_NAME,EVENT_RATING) values (EVENT_SEQ.NEXTVAL,?,?,?)"), //

		SQL_REMOVE("delete from EVENT where EVENT_ID = ?"), //

		SQL_TAKE_BY_ID("select EVENT_ID,BASE_PRICE,EVENT_NAME,EVENT_RATING from EVENT where EVENT_ID = ?"), //

		SQL_TAKE_BY_NAME("select EVENT_ID,BASE_PRICE,EVENT_NAME,EVENT_RATING from EVENT where EVENT_NAME = ?");

		final String query;

		EventDaoSqlQuery(String query) {
			this.query = query;
		}

	}

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Long save(Event event) {

		KeyHolder holder = new GeneratedKeyHolder();

		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(EventDaoSqlQuery.SQL_SAVE.query,
						new String[] { SqlFields.EVENT_ID.name() });
				ps.setDouble(1, event.getBasePrice());
				ps.setString(2, event.getEventName());
				ps.setString(3, event.getRating().name());

				return ps;
			}
		}, holder);

		return holder.getKey().longValue();

	}

	@Override
	public void remove(Long eventId) {
		jdbcTemplate.update(EventDaoSqlQuery.SQL_REMOVE.query, eventId);
	}

	@Override
	public Event getById(Long eventId) {

		try {
			return jdbcTemplate.queryForObject(EventDaoSqlQuery.SQL_TAKE_BY_ID.query, new Object[] { eventId },
					new EventRowMapper());
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public List<Event> getAll() {
		return jdbcTemplate.query(EventDaoSqlQuery.SQL_TAKE_ALL.query, new EventRowMapper());
	}

	@Override
	public Event getByName(String name) {
		try {
			return jdbcTemplate.queryForObject(EventDaoSqlQuery.SQL_TAKE_BY_NAME.query, new Object[] { name },
					new EventRowMapper());

		} catch (EmptyResultDataAccessException e) {
			return null;

		}
	}

}
