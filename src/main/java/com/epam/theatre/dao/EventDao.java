package com.epam.theatre.dao;

import com.epam.theatre.domain.Event;

public interface EventDao extends AbstractDomainObjectDao<Event> {

	Event getByName(String name);

}
