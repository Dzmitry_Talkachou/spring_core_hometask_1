package com.epam.theatre.service;

import com.epam.theatre.domain.Customer;

public interface CustomerService extends AbstractDomainObjectService<Customer> {

	Customer takeByEmail(String email);

	Customer takeByEmailPassword(String email, String password);

	void updateById(Long customerId, Customer customer);

	void updateByPasswordAndId(Long customerId, String requestParameter, String newPassword);

}
