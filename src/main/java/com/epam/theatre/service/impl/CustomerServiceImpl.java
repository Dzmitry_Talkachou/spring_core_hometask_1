package com.epam.theatre.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.theatre.dao.CustomerDao;
import com.epam.theatre.domain.Customer;
import com.epam.theatre.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDao userDao;

	@Override
	public Long save(Customer user) {
		return userDao.save(user);
	}

	@Override
	public void remove(Long userId) {
		userDao.remove(userId);

	}

	@Override
	public Customer getById(Long userId) {
		return userDao.getById(userId);
	}

	@Override
	public List<Customer> getAll() {
		return userDao.getAll();
	}

	@Override
	public Customer takeByEmail(String email) {
		return userDao.getUserByEmail(email);
	}

	@Override
	public Customer takeByEmailPassword(String email, String password) {
		return userDao.takeByEmailPassword(email, password);
	}

	@Override
	public void updateById(Long customerId, Customer customer) {
		userDao.updateById(customerId, customer);

	}

	@Override
	public void updateByPasswordAndId(Long customerId, String requestParameter, String newPassword) {
		//userDao.updateByPasswordAndId(customerId, requestParameter, newPassword);

	}

}
