package com.epam.theatre.service;

import com.epam.theatre.domain.Event;

public interface EventService extends AbstractDomainObjectService<Event> {

	Event getByName(String name);

}
