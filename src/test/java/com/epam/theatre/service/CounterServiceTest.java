package com.epam.theatre.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.theatre.dao.CounterDao;
import com.epam.theatre.domain.Counter;
import com.epam.theatre.service.impl.CounterServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CounterServiceTest {
	@Mock
	private CounterDao counterDaoMock;

	@InjectMocks
	private CounterServiceImpl counterService;

	@Test
	public void getByIdCounterServiceTest() {
		counterService.getById(1L);
		verify(counterDaoMock).getById(1L);
	}

	@Test
	public void saveCounterServiceTest() {
		Counter counter = new Counter();
		counterService.save(counter);
		verify(counterDaoMock).save(counter);
	}

	@Test
	public void removeCounterServiceTest() {
		counterService.remove(2L);
		verify(counterDaoMock).remove(2L);
	}

	@Test
	public void getAllCounterServiceTest() {
		when(counterDaoMock.getAll()).thenAnswer(new Answer<List<Counter>>() {
			@Override
			public List<Counter> answer(InvocationOnMock invoke) throws Throwable {

				return new ArrayList<Counter>() {

					private static final long serialVersionUID = 1L;

					{
						add(new Counter());
						add(new Counter());
						add(new Counter());
					}
				};
			};
		});

		List<Counter> counters = counterService.getAll();
		verify(counterDaoMock).getAll();
		assertSame(3, counters.size());

	}

	@Test
	public void takeByClassNameAndClassIdCounterServiceTest() {
		counterService.takeByClassNameAndClassId("Event", "Test", 1L);
		verify(counterDaoMock).takeByClassNameAndClassId("Event", "Test", 1L);
	}

	@Test
	public void updateCounterServiceTest() {
		counterService.update(1L, 2L);
		verify(counterDaoMock).update(1L, 2L);
	}

	@Test
	public void takeAllCountersValueByClassNameCounterServiceTest() {
		counterService.takeAllCountersValueByClassName("Event");
		verify(counterDaoMock).takeAllCountersValueByClassName("Event");
	}

}
