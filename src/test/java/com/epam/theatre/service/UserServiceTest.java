package com.epam.theatre.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.theatre.dao.CustomerDao;
import com.epam.theatre.domain.Customer;
import com.epam.theatre.service.impl.CustomerServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
	@Mock
	private CustomerDao userDaoMock;

	@InjectMocks
	private CustomerServiceImpl userService;

	@Test
	public void getByIdUserServiceTest() {
		userService.getById(1L);
		verify(userDaoMock).getById(1L);
	}

	@Test
	public void saveUserServiceTest() {
		Customer user = new Customer();
		userService.save(user);
		verify(userDaoMock).save(user);
	}

	@Test
	public void removeUserServiceTest() {
		userService.remove(2L);
		verify(userDaoMock).remove(2L);
	}

	@Test
	public void getAllUserServiceTest() {
		when(userDaoMock.getAll()).thenAnswer(new Answer<List<Customer>>() {
			@Override
			public List<Customer> answer(InvocationOnMock invoke) throws Throwable {

				return new ArrayList<Customer>() {

					private static final long serialVersionUID = 1L;

					{
						add(new Customer());
						add(new Customer());
						add(new Customer());
					}
				};
			};
		});

		List<Customer> users = userService.getAll();
		verify(userDaoMock).getAll();
		assertSame(3, users.size());

	}

	@Test
	public void getUserByEmailUserServiceTest() {
		userService.takeByEmail("jimik@mail.ru");
		verify(userDaoMock).getUserByEmail("jimik@mail.ru");
	}

}
