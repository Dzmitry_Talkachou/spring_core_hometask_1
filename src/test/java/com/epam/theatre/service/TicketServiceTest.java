package com.epam.theatre.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.anyLong;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.theatre.dao.TicketDao;
import com.epam.theatre.domain.Ticket;
import com.epam.theatre.service.impl.TicketServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TicketServiceTest {
	@Mock
	private TicketDao ticketDaoMock;
	@InjectMocks
	private TicketServiceImpl ticketService;

	@Test
	public void getByIdTicketServiceTest() {
		ticketService.getById(1L);
		verify(ticketDaoMock).getById(1L);
	}

	@Test
	public void saveTicketServiceTest() {
		Ticket ticket = new Ticket();
		ticketService.save(ticket);
		verify(ticketDaoMock).save(ticket);
	}

	@Test
	public void removeTicketServiceTest() {
		ticketService.remove(2L);
		verify(ticketDaoMock).remove(2L);
	}

	@Test
	public void getAllTicketServiceTest() {
		when(ticketDaoMock.getAll()).thenAnswer(new Answer<List<Ticket>>() {
			@Override
			public List<Ticket> answer(InvocationOnMock invoke) throws Throwable {

				return new ArrayList<Ticket>() {

					private static final long serialVersionUID = 1L;

					{
						add(new Ticket());
						add(new Ticket());
						add(new Ticket());
					}
				};
			};
		});

		List<Ticket> tickets = ticketService.getAll();
		verify(ticketDaoMock).getAll();
		assertSame(3, tickets.size());

	}

	@Test
	public void getTicketsByUserIdTicketServiceTest() {
		when(ticketDaoMock.getTicketsByUserId(anyLong())).thenAnswer(new Answer<List<Ticket>>() {
			@Override
			public List<Ticket> answer(InvocationOnMock invoke) throws Throwable {

				return new ArrayList<Ticket>() {

					private static final long serialVersionUID = 1L;

					{
						add(new Ticket());
						add(new Ticket());
						add(new Ticket());
					}
				};
			};
		});

		List<Ticket> tickets = ticketService.getTicketsByUserId(1L);
		verify(ticketDaoMock).getTicketsByUserId(1L);
		assertSame(3, tickets.size());

	}

	@Test
	public void checkSeatTicketServiceTest() {
		when(ticketDaoMock.checkSeat(anyLong(), anyLong())).thenReturn(true);
		boolean value = ticketService.checkSeat(1L, 12L);
		verify(ticketDaoMock).checkSeat(1L, 12L);
		assertTrue(value);
	}

	@Test
	public void getTicketsQuantityByUserIdTicketServiceTest() {
		when(ticketDaoMock.getTicketsQuantityByUserId(anyLong())).thenReturn(5L);
		long value = ticketService.getTicketsQuantityByUserId(1L);
		verify(ticketDaoMock).getTicketsQuantityByUserId(1L);
		assertSame(5L, value);
	}

	@Test
	public void saveAllTicketServiceTest() {
		Set<Ticket> tickets = new HashSet<Ticket>() {

			private static final long serialVersionUID = 1L;

			{
				add(new Ticket());
				add(new Ticket());
				add(new Ticket());
			}
		};

		ticketService.saveAll(tickets);
		verify(ticketDaoMock).saveAll(tickets);
	}

}
