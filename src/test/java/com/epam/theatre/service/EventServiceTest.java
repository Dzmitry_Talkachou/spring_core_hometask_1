package com.epam.theatre.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.theatre.dao.EventDao;
import com.epam.theatre.domain.Event;
import com.epam.theatre.service.impl.EventServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class EventServiceTest {
	@Mock
	private EventDao eventDaoMock;

	@InjectMocks
	private EventServiceImpl eventService;

	@Test
	public void getByIdEventServiceTest() {
		eventService.getById(1L);
		verify(eventDaoMock).getById(1L);
	}

	@Test
	public void saveEventServiceTest() {
		Event event = new Event();
		eventService.save(event);
		verify(eventDaoMock).save(event);
	}

	@Test
	public void removeEventServiceTest() {
		eventService.remove(2L);
		verify(eventDaoMock).remove(2L);
	}

	@Test
	public void getAllEventServiceTest() {
		when(eventDaoMock.getAll()).thenAnswer(new Answer<List<Event>>() {
			@Override
			public List<Event> answer(InvocationOnMock invoke) throws Throwable {

				return new ArrayList<Event>() {

					private static final long serialVersionUID = 1L;

					{
						add(new Event());
						add(new Event());
						add(new Event());
					}
				};
			};
		});

		List<Event> events = eventService.getAll();
		verify(eventDaoMock).getAll();
		assertSame(3, events.size());

	}

	@Test
	public void getByNameEventServiceTest() {
		eventService.getByName("Vasya");
		verify(eventDaoMock).getByName("Vasya");
	}

}
