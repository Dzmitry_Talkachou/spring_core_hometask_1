package com.epam.theatre.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.theatre.dao.EventScheduleDao;
import com.epam.theatre.domain.EventSchedule;
import com.epam.theatre.service.impl.EventScheduleServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class EventScheduleServiceTest {
	@Mock
	private EventScheduleDao eventScheduleDaoMock;

	@InjectMocks
	private EventScheduleServiceImpl eventScheduleService;

	@Test
	public void getByIdEventScheduleServiceTest() {
		eventScheduleService.getById(1L);
		verify(eventScheduleDaoMock).getById(1L);
	}

	@Test
	public void saveEventScheduleServiceTest() {
		EventSchedule eventSchedule = new EventSchedule();
		eventScheduleService.save(eventSchedule);
		verify(eventScheduleDaoMock).save(eventSchedule);
	}

	@Test
	public void removeEventScheduleServiceTest() {
		eventScheduleService.remove(2L);
		verify(eventScheduleDaoMock).remove(2L);
	}

	@Test
	public void getAllEventScheduleServiceTest() {
		when(eventScheduleDaoMock.getAll()).thenAnswer(new Answer<List<EventSchedule>>() {
			@Override
			public List<EventSchedule> answer(InvocationOnMock invoke) throws Throwable {

				return new ArrayList<EventSchedule>() {

					private static final long serialVersionUID = 1L;

					{
						add(new EventSchedule());
						add(new EventSchedule());
						add(new EventSchedule());
					}
				};
			};
		});

		List<EventSchedule> eventSchedules = eventScheduleService.getAll();
		verify(eventScheduleDaoMock).getAll();
		assertSame(3, eventSchedules.size());

	}

}
