package com.epam.theatre.dao;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.theatre.config.AppConfigCommon;
import com.epam.theatre.domain.Ticket;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfigCommon.class })
public class TicketDaoTest extends AbstractTest {

	@Autowired
	private TicketDao ticketDao;

	@Override
	public IDataSet takeDataSetToInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/test_data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/ticket_data.xml")) };
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDBTestCase() throws FileNotFoundException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), takeDataSetToInsert());
	}

	@Test
	public void saveTicketTest() {
		Ticket ticket = new Ticket() {
			{
				setTicketCost(9999.0);
				setSeat(111L);
				setEventScheduleId(1L);
				setUserId(1L);
				setDiscount(44.0);
			}
		};
		long ticketId = ticketDao.save(ticket);
		assertNotEquals(0, ticketId);
		assertNotNull(ticketDao.getById(ticketId));
	}

	@Test
	public void removeTicketTest() {
		ticketDao.remove(1L);
		assertNull(ticketDao.getById(1L));
	}

	@Test
	public void getByIdTicketTest() {
		assertNotNull(ticketDao.getById(1L));
	}

	@Test
	public void getAllTicketTest() {
		List<Ticket> tickets = ticketDao.getAll();
		assertTrue(tickets.size()==3);
	}

	@Test
	public void takeTicketsByEventScheduleIdTicketTest() {
		List<Ticket> tickets = ticketDao.takeTicketsByEventScheduleId(1L);
		assertTrue(tickets.size()==2);
	}

	@Test
	public void checkSeatTicketTest() {
		assertTrue(ticketDao.checkSeat(55L, 2L));
		assertFalse(ticketDao.checkSeat(52L, 2L));
	}

	@Test
	public void getTicketsQuantityByUserIdTicketTest() {
		assertTrue(ticketDao.getTicketsQuantityByUserId(1L)==3L);
	}

	@Test
	public void saveAllTicketTest() {

		Set<Ticket> tickets = new HashSet<Ticket>() {

			private static final long serialVersionUID = 4054359414350456854L;

			{

				add(new Ticket() {
					{
						setTicketCost(9999.0);
						setSeat(111L);
						setEventScheduleId(1L);
						setUserId(1L);
						setDiscount(44.0);
					}
				});

				add(new Ticket() {
					{
						setTicketCost(9999.0);
						setSeat(111L);
						setEventScheduleId(1L);
						setUserId(1L);
						setDiscount(44.0);
					}
				});

				add(new Ticket() {
					{
						setTicketCost(9999.0);
						setSeat(111L);
						setEventScheduleId(1L);
						setUserId(1L);
						setDiscount(44.0);
					}
				});

				add(new Ticket() {
					{
						setTicketCost(9999.0);
						setSeat(111L);
						setEventScheduleId(1L);
						setUserId(1L);
						setDiscount(44.0);
					}
				});

			}
		};

		ticketDao.saveAll(tickets);
		
		assertTrue(ticketDao.getAll().size()==7);

	}
}
