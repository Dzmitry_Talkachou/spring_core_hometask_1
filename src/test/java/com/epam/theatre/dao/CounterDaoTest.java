package com.epam.theatre.dao;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.theatre.config.AppConfigCommon;
import com.epam.theatre.domain.Counter;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfigCommon.class })
public class CounterDaoTest extends AbstractTest {

	@Autowired
	private CounterDao counterDao;

	@Override
	public IDataSet takeDataSetToInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/test_data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/counter_data.xml")) };
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDBTestCase() throws FileNotFoundException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), takeDataSetToInsert());
	}

	@Test
	public void saveCounterTest() {
		Counter counter = new Counter() {
			{
				setCounterName("Counter3");
				setCounterValue(10L);
				setTypeId(1L);
				setTypeName("Event");

			}
		};
		long counterId = counterDao.save(counter);
		assertNotEquals(0, counterId);
		assertNotNull(counterDao.getById(counterId));
	}

	@Test
	public void remveCounterTest() {
		counterDao.remove(1L);
		assertNull(counterDao.getById(1L));
	}

	@Test
	public void getByIdCounterTest() {
		assertNotNull(counterDao.getById(1L));
	}

	@Test
	public void getAllTicketTest() {
		List<Counter> counters = counterDao.getAll();
		assertTrue(counters.size() == 3);
	}

	@Test
	public void takeByClassNameAndClassIdCounterTest() {
		assertNotNull(counterDao.takeByClassNameAndClassId("Event", "Counter1", 1L));
	}

	@Test
	public void updateCounterTest() {

		counterDao.update(1L, 10L);
		assertTrue(counterDao.getById(1L).getCounterValue() == 10L);

	}

	@Test
	public void takeAllCountersValueByClassNameCounterTest() {

		assertTrue(counterDao.takeAllCountersValueByClassName("Counter1") == 10L);

	}

}
