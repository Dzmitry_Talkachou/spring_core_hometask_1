package com.epam.theatre.dao;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.time.LocalDateTime;
import java.util.List;

import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.theatre.config.AppConfigCommon;
import com.epam.theatre.domain.EventSchedule;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfigCommon.class })
public class EventScheduleDaoTest extends AbstractTest {

	@Autowired
	private EventScheduleDao eventScheduleDao;

	@Override
	public IDataSet takeDataSetToInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/test_data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/event_schedule_data.xml")) };
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDBTestCase() throws FileNotFoundException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), takeDataSetToInsert());
	}

	@Test
	public void saveEventScheduleTest() {
		EventSchedule eventSchedule = new EventSchedule() {
			{
				setEventId(2L);
				setAuditoriumId(1L);
				setEventDate(LocalDateTime.now());

			}
		};
		long eventScheduleId = eventScheduleDao.save(eventSchedule);
		assertNotEquals(0, eventScheduleId);
		assertNotNull(eventScheduleDao.getById(eventScheduleId));
	}

	@Test
	public void removeEventScheduleTest() {
		eventScheduleDao.remove(4L);
		assertNull(eventScheduleDao.getById(4L));
	}

	@Test
	public void getByIdEventScheduleTest() {
		assertNotNull(eventScheduleDao.getById(1L));
	}

	@Test
	public void getAllEventScheduleTest() {
		List<EventSchedule> eventSchedules = eventScheduleDao.getAll();
		assertTrue(eventSchedules.size() == 5);
	}

}
