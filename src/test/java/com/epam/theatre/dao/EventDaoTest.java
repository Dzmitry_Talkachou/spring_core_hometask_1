package com.epam.theatre.dao;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.theatre.config.AppConfigCommon;
import com.epam.theatre.domain.Event;
import com.epam.theatre.domain.EventRating;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfigCommon.class })
public class EventDaoTest extends AbstractTest {

	@Autowired
	private EventDao eventDao;

	@Override
	public IDataSet takeDataSetToInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/test_data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/event_data.xml")) };
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDBTestCase() throws FileNotFoundException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), takeDataSetToInsert());
	}

	@Test
	public void saveEventTest() {
		Event event = new Event() {
			{
				setBasePrice(100.0);
				setEventName("Event8");
				setRating(EventRating.MID);
			}
		};
		long eventId = eventDao.save(event);
		assertNotEquals(0, eventId);
		assertNotNull(eventDao.getById(eventId));
	}

	@Test
	public void removeEventTest() {
		eventDao.remove(4L);
		assertNull(eventDao.getById(4L));
	}

	@Test
	public void getByIdEventTest() {
		assertNotNull(eventDao.getById(1L));
	}

	@Test
	public void getByNameEventTest() {
		assertNotNull(eventDao.getByName("Event2"));
	}

	@Test
	public void getAllEventTest() {
		List<Event> events = eventDao.getAll();
		assertTrue(events.size()==6);
	}

}
