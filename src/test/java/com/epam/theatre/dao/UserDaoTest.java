package com.epam.theatre.dao;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDate;

import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.theatre.config.AppConfigCommon;
import com.epam.theatre.domain.Customer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfigCommon.class })
public class UserDaoTest extends AbstractTest {

	@Autowired
	private CustomerDao userDao;

	@Override
	public IDataSet takeDataSetToInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/test_data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/user_data.xml")) };
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDBTestCase() throws FileNotFoundException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), takeDataSetToInsert());
	}

	@Test
	public void saveUserTest() {
		Customer user = new Customer() {
			{
				setBirthDay(LocalDate.now());
				setEmail("user@epam.com");
				setFirstName("Nikinor");
				setPassword("1122233");
				setLastName("Pochemushkin");

			}
		};
		long userId = userDao.save(user);
		assertNotEquals(0, userId);
		assertNotNull(userDao.getById(userId));
	}

	@Test
	public void removeUserTest() {
		userDao.remove(4L);
		assertNull(userDao.getById(4L));
	}

	@Test
	public void getByIdUserTest() {
		assertNotNull(userDao.getById(1L));
	}

	@Test
	public void getAllUserTest() {
		assertTrue(userDao.getAll().size() == 4);
	}

}
