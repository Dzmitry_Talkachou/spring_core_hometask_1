package com.epam.theatre.dao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

public abstract class AbstractTest {
    @Autowired
    DataSource dataSource;
    Connection con;


    public IDatabaseConnection takeConnection() throws Exception {
	con = DataSourceUtils.getConnection(dataSource);
	DatabaseMetaData databaseMetaData = con.getMetaData();
	IDatabaseConnection connection = new DatabaseConnection(con, databaseMetaData.getUserName().toUpperCase());
	connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());
	return connection;
    }

    public abstract IDataSet takeDataSetToInsert() throws DataSetException, FileNotFoundException;

    public IDataSet takeDataSetToDelete() throws DataSetException, FileNotFoundException {
	return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/delete_data.xml"));
    }

    public abstract void configureDBTestCase() throws DatabaseUnitException, SQLException, Exception;

    @After
    public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
	DatabaseOperation.DELETE_ALL.execute(takeConnection(), takeDataSetToDelete());
	releaseConnection();
    }

    @After
    public void releaseConnection() {
	DataSourceUtils.releaseConnection(con, dataSource);
    }
}